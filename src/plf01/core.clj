(ns plf01.core)
;;<
(defn función-<-1
  [a]
  (< a))
(defn función-<-2
  [a b]
  (< a b))
(defn función-<-3
  [a b c]
  (< a b c))
(función-<-1 5)
(función-<-2 5 10)
(función-<-3 5 10 15)
;;<=
(defn función-<=-1
  [a]
  (<= a))
(defn función-<=-2
  [a b]
  (<= a b))
(defn función-<=-3
  [a b c]
  (<= a b c))
(función-<=-1 5)
(función-<=-2 10 5)
(función-<=-3 15 10 10)
;;==
(defn función-==-1
  [a]
  (== a))
(defn función-==-2
  [a b]
  (== a b))
(defn función-==-3
  [a b c]
  (== a b c))
(función-==-1 5)
(función-==-2 5 1)
(función-==-3 5 5 5)
;;>
(defn función->-1
  [a]
  (> a))
(defn función->-2
  [a b]
  (> a b))
(defn función->-3
  [a b c]
  (> a b c))
(función->-1 5)
(función->-2 10 5)
(función->-3 10 5 1)
;;>=
(defn función->=-1
  [a]
  (>= a))
(defn función->=-2
  [a b]
  (>= a b))
(defn función->=-3
  [a b c]
  (>= a b c))
(función->=-1 5)
(función->=-2 5 5)
(función->=-3 10 10 5)
;;assoc
(defn función-assoc-1
  [a b zs]
  (assoc a b zs))
(defn función-assoc-2
  [a b c d e]
  (assoc a b c d e))
(defn función-assoc-3
  [a b c d e f h]
  (assoc a b c d e f h))
(función-assoc-1 {:name "Isai" :sex "Masculine"} :name "abi")
(función-assoc-2 {:name "Isai" :ap1 "Sanchez"} :name "abi" :ap1 "lopez") 
(función-assoc-3 {:name "Isai" :ap1 "Sanchez" :ap3 "lopez"} :name "abi" :ap1 "lopez" :ap2 "Sanchez")
;;assoc-in
(defn función-assoc-in-1
  [a b c]
  (assoc-in a b c))
(defn función-assoc-in-2
  [a b c]
  (assoc-in a b c))
(defn función-assoc-in-3
  [a b c]
  (assoc-in a b c))
(función-assoc-in-1 {:name "Isai"} "Isai" "abi")
(función-assoc-in-2 {"Isai" {:ap1 "Sanchez"}} "martinez" "lopez")
(función-assoc-in-3 {"Isai" {"Sanchez" {:ap2 "lopez"}}} "abi" "sanchez")
;;concat
(defn función-contat-1
  [a]
  (concat a))
(defn función-contat-2
  [a]
  (concat a a))
(defn función-contat-3
  [a b c]
  (concat a b c))
(función-contat-1 "")
(función-contat-2 "abi")
(función-contat-3 "Abi" "Isai" "007")
;;conj
(defn función-conj-1
  [a b]
  (conj a b))
(defn función-conj-2
  [a b]
  (conj a b))
(defn función-conj-3
  [a b]
  (conj a b))
(función-conj-1 ["Camino" "Ruta"] ["007"])
(función-conj-2 ["Langosta" "Langosta"] ["Langosta" "Cangrejo"])
(función-conj-3 ["Camino" "Ruta"] ["Comida" "Agua" "Suministros"])
;;cons
(defn función-cons-1
  [a b]
  (cons a b))
(defn función-cons-2
  [a b]
  (cons a b))
(defn función-cons-3
  [a b]
  (cons a b))
(función-cons-1 nil (list 6,7,8,9,0))
(función-cons-2 5 (list 6,7,8,9,0))
(función-cons-3 [52 25] (list 6,7,8,9,0))
;;contains?
(defn función-contains?-1
  [a b]
  (contains? a b))
(defn función-contains?-2
  [a b]
  (contains? a b))
(defn función-contains?-3
  [a b]
  (contains? a b))
(función-contains?-1 [:a :b :c] :a)
(función-contains?-2 {:a "a" :b "b" :c "c"} :c)
(función-contains?-3 #{:x :y :z} :x)
;;count
(defn función-count-1
  [a]
  (count a))
(defn función-count-2
  [a]
  (count a))
(defn función-count-3
  [a]
  (count a))
(función-count-1 [])
(función-count-2 ["x" "y" "z"])
(función-count-3 ["x" "y" "z" 1 2 3])
;;disj
(defn función-disj-1
  [a b c d]
  (disj a b c d))
(defn función-disj-2
  [a b c]
  (disj a b c))
(defn función-disj-3
  [a b c d]
  (disj a b c d))
(función-disj-1 #{1 2 3 4 5 6 7 8 9 0} 2 3 4)
(función-disj-2 #{"a" "b" "c" "d" "e"} "a" "b")
(función-disj-3 #{:a :b :c :d :e :f} :a :b :c)
;;dissoc
(defn función-dissoc-1
  [a b]
  (dissoc a b))
(defn función-dissoc-2
  [a b c]
  (dissoc a b c))
(defn función-dissoc-3
  [a b c d]
  (dissoc a b c d))
(función-dissoc-1 {:x :y :z :a :b :c} :x)
(función-dissoc-2 {:a :c :e :d :f :g} :a :b)
(función-dissoc-3 {:2 :4 :5 :6 :7 :8} :1 :2 :3)
;;distinct
(defn función-distinct-1
  [x]
  (distinct x))
(defn función-distinct-2
  [x]
  (distinct x))
(defn función-distinct-3
  [x]
  (distinct x))
(función-distinct-1 [1 2 3])
(función-distinct-2 [1 2 3 1 2 3])
(función-distinct-3 [1 2 3 1 2 54 4 3])
;;distinct?
(defn función-distinct?-1
  [a]
  (distinct? a))
(defn función-distinct?-2
  [a]
  (distinct? a))
(defn función-distinct?-3
  [a b zs]
  (distinct? a b zs))
(función-distinct?-1 2)
(función-distinct?-2 1)
(función-distinct?-3 1 2 3)
;;drop-last
(defn función-drop-last-1
  [a b]
  (drop-last a b))
(defn función-drop-last-2
  [a b]
  (drop-last a b))
(defn función-drop-last-3
  [a b]
  (drop-last a b))
(función-drop-last-1 0 [1 2 3 4 5])
(función-drop-last-2 1 [1 2 3 4 5])
(función-drop-last-3 4 [1 2 3 4 5])
;;empty
(defn función-empty-1
  [a]
  (empty a))
(defn función-empty-2
  [a]
  (empty a))
(defn función-empty-3
  [a]
  (empty a))
(función-empty-1 #{1 2 3 4 5 6 7})
(función-empty-2 [1 12 1 12 1 1])
(función-empty-3 (list 34 34635 63 5 63))
;;empty?
(defn función-empty?-1
  [a]
  (empty? a))
(defn función-empty?-2
  [a]
  (empty? a))
(defn función-empty?-3
  [a]
  (empty? a))
(función-empty?-1 [23 3])
(función-empty?-2 {})
(función-empty?-3 (list 2 3 4))
;;even?
(defn función-even?-1
  [a]
  (even? a))
(defn función-even?-2
  [a]
  (even? a))
(defn función-even?-3
  [a]
  (even? a))
(función-even?-1 2)
(función-even?-2 651)
(función-even?-3 6564)
;;false?
(defn función-false?-1
  [a]
  (false? a))
(defn función-false?-2
  [a]
  (false? a))
(defn función-false?-3
  [a]
  (false? a))
(función-false?-1 true)
(función-false?-2 "true")
(función-false?-3 false)
;;find
(defn función-find-1
  [a b]
  (find a b))
(defn función-find-2
  [a b]
  (find a b))
(defn función-find-3
  [a b]
  (find a b))
(función-find-1 {:x \x :y "y" :num 123 :true true} :a)
(función-find-2 {:x \x :y "y" :num 123 :true true} :y)
(función-find-3 {:x \x :y "y" :num 123 :true true} :true)
;;first
(defn función-first-1
  [a]
  (first a))
(defn función-first-2
  [a]
  (first a))
(defn función-first-3
  [a]
  (first a))
(función-first-1 ["abi" \a true 123 :asd])
(función-first-2 '(23 23 3 424 24 2 42))
(función-first-3 {:asd "asd" :num 123})
;;flatten
(defn función-flatten-1
  [a]
  (flatten a))
(defn función-flatten-2
  [a]
  (flatten a))
(defn función-flatten-3
  [a]
  (flatten a))
(función-flatten-1 [\a \a \1 \5 \@])
(función-flatten-2 {:a "a" :A "A"})
(función-flatten-3 #{"obtener" "premio" "oscar"})
;;frequencies función-
(defn función-frequencies-1
  [x]
  (frequencies x))
(defn función-frequencies-2
  [x]
  (frequencies x))
(defn función-frequencies-3
  [x]
  (frequencies x))
(función-frequencies-1 [1 1 1 1 1 243 242 35 236 245])
(función-frequencies-2 {:a "a" :b "b" :c "a"})
(función-frequencies-3 #{\a \3 \4 \@})
;;get
(defn función-get-1
  [a b]
  (get a b))
(defn función-get-2
  [a b]
  (get a b))
(defn función-get-3
  [a b c]
  (get a b c))
(función-get-1 ["a" "b" "c" 1 2 3] 3)
(función-get-2 [1 2 3 4] 1)
(función-get-3 {:a \a :b \b :c \c :v \v :e \e} :a "not")
;;get-in
(defn función-get-in-1
  [a b]
  (get-in a b))
(defn función-get-in-2
  [a b]
  (get-in a b))
(defn función-get-in-3
  [a b c]
  (get-in a b c))
(función-get-in-1 {:name "Abi"} [:name])
(función-get-in-2 {:name "Abi" :info {:id 007 :sex "M"}} [:info :sex])
(función-get-in-3 {:name "Abi" :info {:id 007 :sex "M"}} [:info :area] "no info")
;;into
(defn función-into-1
  [a b]
  (into a b))
(defn función-into-2
  [a b]
  (into a b))
(defn función-into-3
  [a b]
  (into a b))
(función-into-1 {:4 4} {:1 1})
(función-into-2 {:a 1} [{:x 1 :y 2}])
(función-into-3 {:name "USER"} [{:per nil :autori nil} {:a "report" :b "xD"}])
;;key
(defn función-key-1
  [a]
  (key a))
(defn función-key-2
  [a]
  (key a))
(defn función-key-3
  [a]
  (key a))
(función-key-1 (first {:a 1 :b 2 :c 3 :d 4 :e 5 :f 6}))
(función-key-2 (first {:7 \7}))
(función-key-3 (first {:name "abis"}))
;;keys
(defn función-keys-1
  [a]
  (keys a))
(defn función-keys-2
  [b]
  (keys b))
(defn función-keys-3
  [c]
  (keys c))
(función-keys-1 {:name "abi"})
(función-keys-2 {:name "Abi" :info {:estado "Oaxaca"}})
(función-keys-3 {:name "Abi" :est ["oax" "cdmx"] :info "extra xD"})
;;max
(defn función-max-1
  [a]
  (max a))
(defn función-max-2
  [a b]
  (max a b))
(defn función-max-3
  [a b c d]
  (max a b c d))
(función-max-1 1)
(función-max-2 7 7)
(función-max-3 8 8 9 100)
;;merge
(defn función-merge-1
  [a b]
  (merge a b))
(defn función-merge-2
  [a b c]
  (merge a b c))
(defn función-merge-3
  [a b c d e]
  (merge a b c d e))
(función-merge-1 {:name "xxxx"} {:aux "aux"})
(función-merge-2 {:name "xxxx"} {:aux "middle"} {:ap1 "llll"})
(función-merge-3 {:starr "inicio"} {:name "xxxx"} {:name2 "yyyy"} {:name3 "zzz"} {:name4 "tengo sueño"})
;;min
(defn función-min-1
  [a]
  (min a))
(defn función-min-2
  [a b]
  (min a b))
(defn función-min-3
  [a b c]
  (min a b c))
(función-min-1 1)
(función-min-2 1 7)
(función-min-3 1 -15 84)
;;neg?
(defn función-neg?-1
  [a]
  (neg? a))
(defn función-neg?-2
  [a]
  (neg? a))
(defn función-neg?-3
  [a]
  (neg? a))
(función-neg?-1 -5)
(función-neg?-2 5)
(función-neg?-3 (* -1 -1))
;;nil?
(defn función-nil?-1
  [x]
  (nil? x))
(defn función-nil?-2
  [x]
  (nil? x))
(defn función-nil?-3
  [x]
  (nil? x))
(función-nil?-1 nil)
(función-nil?-2 [])
(función-nil?-3 0)
;;not-empty
(defn función-not-empty-1
  [a]
  (empty a))
(defn función-not-empty-2
  [a]
  (empty a))
(defn función-not-empty-3
  [a]
  (empty a))
(función-not-empty-1 #{1 2 3 4 5 6 7})
(función-not-empty-2 [1 12 1 12 1 1])
(función-not-empty-3 (list 34 34635 63 5 63))
;;nth
(defn función-nth-1
  [a b]
  (nth a b))
(defn función-nth-2
  [a b c]
  (nth a b c))
(defn función-nth-3
  [a b c]
  (nth a b c))
(función-nth-1 [1 2 3] 0)
(función-nth-2 [1 2 3 4 5 6 7 8 9 0 999] 999 "no encotrado xD")
(función-nth-3 [1 2 3] 1 "perdido")
;;odd?
(defn función-odd?-1
  [a]
  (odd? a))
(defn función-odd?-2
  [a]
  (odd? a))
(defn función-odd?-3
  [a]
  (odd? a))
(función-odd?-1 7)
(función-odd?-2 68)
(función-odd?-3 -5)
;;partition
(defn función-partition-1
  [a b]
  (partition a b))
(defn función-partition-2
  [a b c]
  (partition a b c))
(defn función-partition-3
  [a b c]
  (partition a b c))
(función-partition-1 2 (range 10))
(función-partition-2 2 2 (range 15))
(función-partition-3 2 1 (range 57))
;;partition-all
(defn función-partition-all-1
  [a b]
  (partition-all a b))
(defn función-partition-all-2
  [a b c]
  (partition-all a b c))
(defn función-partition-all-3
  [a b c]
  (partition-all a b c))
(función-partition-all-1 2 (range 10))
(función-partition-all-2 2 2 (range 15))
(función-partition-all-3 2 1 (range 57))
;;peek
(defn función-peek-1
  [a]
  (peek a))
(defn función-peek-2
  [a]
  (peek a))
(defn función-peek-3
  [a]
  (peek a))
(función-peek-1 [1 2 3])
(función-peek-2 '(1 2 3 5 8 9))
(función-peek-3 (list 9 2 3))
;;pop
(defn función-pop-1
  [a]
  (pop a))
(defn función-pop-2
  [a]
  (pop a))
(defn función-pop-3
  [a]
  (pop a))
(función-pop-1 [1 2 3])
(función-pop-2 '(1 2 3 5 8 9))
(función-pop-3 (list 9 2 3))
;;pos?
(defn función-pos?-1
  [a]
  (pos? a))
(defn función-pos?-2
  [a]
  (pos? a))
(defn función-pos?-3
  [a]
  (pos? a))
(función-pos?-1 0)
(función-pos?-2 1)
(función-pos?-3 -4)
;;quot
(defn función-quot-1
  [a b]
  (quot a b))
(defn función-quot-2
  [a b]
  (quot a b))
(defn función-quot-3
  [a b]
  (quot a b))
(función-quot-1 2 10)
(función-quot-2 10 5)
(función-quot-3 8 2)
;;range
(defn función-range-1
  [x]
  (range x))
(defn función-range-2
  [x y]
  (range x y))
(defn función-range-3
  [x y z]
  (range x y z))
(función-range-1 7)
(función-range-2 7 35)
(función-range-3 7 999 46)
;;rem
(defn función-rem-1
  [a b]
  (rem a b))
(defn función-rem-2
  [a b]
  (rem a b))
(defn función-rem-3
  [a b]
  (rem a b))
(función-rem-1 9 10)
(función-rem-2 -10 3)
(función-rem-3 3 26)
;;repeat
(defn función-repeat-1
  [a b]
  (repeat a b))
(defn función-repeat-2
  [a b]
  (repeat a b))
(defn función-repeat-3
  [a b]
  (repeat a b))
(función-repeat-1 5 1)
(función-repeat-2 5 "x")
(función-repeat-3 5 [1])
;;replace
(defn función-replace-1
  [a b]
  (replace a b))
(defn función-replace-2
  [a b]
  (replace a b))
(defn función-replace-3
  [a b]
  (replace a b))
(función-replace-1 [:uno :dos :tres] [0 1 2])
(función-replace-2 ["a" "b" "c"] [2])
(función-replace-3 {2 :a, 4 :b} [1 2 3 4])
;;rest
(defn función-rest-1
  [a]
  (rest a))
(defn función-rest-2
  [a]
  (rest a))
(defn función-rest-3
  [a]
  (rest a))
(función-rest-1 {:id 000 :name "abis"})
(función-rest-2 [1 2 3 4 5])
(función-rest-3 (list \x \D))
;;select-keys
(defn función-select-keys-1
  [a b]
  (select-keys a b))
(defn función-select-keys-2
  [a b]
  (select-keys a b))
(defn función-select-keys-3
  [a b]
  (select-keys a b))
(función-select-keys-1 {:z \z :y \y :x \x} [:x])
(función-select-keys-2 {:x \x :d \D} [:d])
(función-select-keys-3 {:a "a" :b "b" :c "c"} [:x])
;;shuffle
(defn función-shuffle-1
  [a]
  (shuffle a))
(defn función-shuffle-2
  [a]
  (shuffle a))
(defn función-shuffle-3
  [a]
  (shuffle a))
función-shuffle-1 [1 2 3 4 5]
función-shuffle-2 #{1 2 3 4 5}
función-shuffle-3 {:1 1 :2 2 :3 3 :4 4}
;;sort
(defn función-sort-1
  [a]
  (sort a))
(defn función-sort-2
  [a]
  (sort a))
(defn función-sort-3
  [a b]
  (sort a b))
(función-sort-1 [615 8 1 3 498 315 684 651])
(función-sort-2 {:7 "-7" :3 "-3" :5 "-5" :0 "-0"})
(función-sort-3 > [7 3 7 8 9 4 1 5])
;;split-at
(defn función-split-at-1
  [a b]
  (split-at a b))
(defn función-split-at-2
  [a b]
  (split-at a b))
(defn función-split-at-3
  [a b]
  (split-at a b))
(función-split-at-1 2 [1 3 5 7 9 3 5 7 7 7 7 7])
(función-split-at-2 2 [1 2 3 2 4 5])
(función-split-at-3 3 (list 1 2 9 4 5 4 3 2 1))
;;str
(defn función-str-1
  [a]
  (str a))
(defn función-str-2
  [a b c]
  (str a b c))
(defn función-str-3
  [a b c d e]
  (str a b c d e))
(función-str-1 007)
(función-str-2 123 "123" \a)
(función-str-3 nil 5 nil 123 "x")
;;subs
(defn función-subs-1
  [a b c]
  (subs a b c))
(defn función-subs-2
  [a b]
  (subs a b))
(defn función-subs-3
  [a b c]
  (subs a b c))
(función-subs-1 "abimael isai lopez Sanchez" 2 5)
(función-subs-2 "abimael isai lopez Sanchez" 5 )
(función-subs-3 "abimael isai lopez Sanchez" 10 15)
;;subvec
(defn función-subvec-1
  [a b]
  (subvec a b))
(defn función-subvec-2
  [a b c]
  (subvec a b c))
(defn función-subvec-3
  [a b c]
  (subvec a b c))
(función-subvec-1 [38 94 73 95 47 38 39 5] 2)
(función-subvec-2 [38 94 73 95 47 38 39 5] 2 5)
(función-subvec-3 [1 1 1 1 1 1 1 1 1 1 1 1 1 1] 7 10)
;;take
(defn función-take-1
  [a b]
  (take a b))
(defn función-take-2
  [a b]
  (take a b))
(defn función-take-3
  [a b]
  (take a b))
(función-take-1 2 [1 2 3])
(función-take-2 1 [])
(función-take-3 5 [0 0 0 0 0 0 0 0 0 0 0 0 ])
;;true?
(defn función-true?-1
  [a]
  (true? a))
(defn función-true?-2
  [a]
  (true? a))
(defn función-true?-3
  [a]
  (true? a))
(función-true?-1 true)
(función-true?-2 (= 1 2))
(función-true?-3 (> 3.0001 3))
;;val
(defn función-val-1
  [a]
  (val a))
(defn función-val-2
  [a]
  (val a))
(defn función-val-3
  [a]
  (val a))
(función-val-1 (first {:one :two}))
(función-val-2 (first {:k :47}))
(función-val-3 (first {"pop" "rap"}))
;;vals
(defn función-vals-1
  [a]
  (vals a))
(defn función-vals-2
  [a]
  (vals a))
(defn función-vals-3
  [a]
  (vals a))
(función-vals-1 {:name "abi"})
(función-vals-2 {:name "Abi" :info {:estado "Oaxaca"}})
(función-vals-3 {:name "Abi" :est ["oax" "cdmx"] :info "extra xD"})
;;zero?
(defn función-zero?-1
    [a]
    (zero? a))
(defn función-zero?-2
  [a]
  (zero? a))
(defn función-zero?-3
  [a]
  (zero? a))
(función-zero?-1 0)
(función-zero?-2 (quot 0 1))
(función-zero?-3 (mod 10 5))
;;zipmap
(defn función-zipmap-1
  [a b]
  (zipmap a b))
(defn función-zipmap-2
  [a b]
  (zipmap a b))
(defn función-zipmap-3
  [a b]
  (zipmap a b))
(función-zipmap-1 [:uno :dos :tres] [1 2 3])
(función-zipmap-2 [:a :b :c] [\a \b \c])
(función-zipmap-3 [:t :f :a] [true false \@ 1])
;;End xD